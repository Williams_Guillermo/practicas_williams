package com.example.williams.practicas;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button Acelerometro,BVibra,BProximinidad,Pelota;

    MiPelota dibujo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dibujo = new MiPelota(this);
        setContentView(dibujo);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        Acelerometro =(Button)findViewById(R.id.btnAcelerometro);
        BProximinidad = (Button)findViewById(R.id.btnProximidad);
        Pelota=(Button)findViewById(R.id.btnPelota);
        BVibra =(Button)findViewById(R.id.btnVibrar);
        final Vibrator vibrator =(Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        BVibra = (Button)findViewById(R.id.btnVibrar);
        BVibra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(600);
            }
        });



        Acelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        BProximinidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, SensorProximidad.class);
                startActivity(intent);
            }
        });

        Pelota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        MainActivity.this, MiPelota.class);
                startActivity(intent);
            }
        });
    }
}
